#!/bin/sh

# Script to run a DNS lookup for a specified APN (FQDN), against one or more DNS servers
# If the node address ($1) is present in the output, the text "UP" is returned, otherwise nothing.
#
# Intended for use as a BIG-IP external monitors
#
# The first two arguments are supplied automatically by bigd for all external monitors:
# $1 = IP (::ffff:nnn.nnn.nnn.nnn notation, or hostname)
# $2 = port (decimal, host byte order) (not used by this monitor)
#
# These additional arguments are supplied via the "args" parameter in the external monitor definition
# $3 = APN name
# $4...= DNS1 DNS2 DNS3 etc

# monitor IP PORT APN DNS1 [DNS2] [DNS3] [DNS4]

# If the environment variable DEBUG is defined, logging will be sent to local0.debug (/var/log/ltm)

pidfile="/var/run/$MONITOR_NAME.$1..$2.pid"
if [ -f $pidfile ]; then
    #debug "Killing previous instance (pid $(cat $pidfile)) of this monitor"
    kill -9 -$(cat $pidfile) >/dev/null 2>&1
fi
echo "$$" >$pidfile

node_ip=$(echo $1 | sed 's/::ffff://')
FQDN=$3

# Iterate through one or more DNS servers, stopping after the first successful one
shift 3
logger -p local0.error " Script running "
while [ ! $result ]; do
    #debug "Running dig +short +timeout=1 @$1 $FQDN"
    output=$(dig +short +retries=0 +timeout=1 @$1 $FQDN 2>/dev/null | grep -v '^;')
    if [ "$output" ]; then
        # debug "Dig returned '$output', checking that it matches the node IP address"

        for line in $output; do
            if [[ $line == "$node_ip" ]]; then
                # debug Output matches node IP - marking UP
                echo UP
                break
            # else
            # debug Output did not match - continuing with remaining DNS servers
            fi
        done
    fi

    shift
    if [ ! $1 ]; then break; fi
done

rm $pidfile
